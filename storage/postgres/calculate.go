package postgres

import (
	pb "add_service/protos"
	"context"
	"fmt"

	"github.com/jackc/pgx/v5/pgxpool"
)

type AddService struct {
	db *pgxpool.Pool
	pb.UnimplementedCalculatorServicesServer
}

func NewAddService(db *pgxpool.Pool) *AddService {
	return &AddService{
		db: db,
	}
}

func (a *AddService) Addition(ctx context.Context, createNum *pb.CreateNumber) (*pb.Result, error) {
	var result = pb.Result{}

	fmt.Println("request is: ", createNum)

	query := `insert into numbers
	 (first_number, second_number) 
	 values ($1, $2) returning first_number=first_number+second_number `

	if err := a.db.QueryRow(ctx, query, createNum.FirstNumber, createNum.SecondNumber).Scan(
		&result.Result,
	); err != nil {
		return nil, err
	}

	return &result, nil

}
