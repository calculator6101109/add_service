package storage


import (
	"context"
	pb "add_service/protos"
)

type IStorage interface {
	Close()
	Add() IAddStorage
}

type IAddStorage interface {
	Create(context.Context, *pb.CreateNumber) (*pb.Result, error)
}
