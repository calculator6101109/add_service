package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"net"
	"add_service/config"
	"add_service/storage/postgres"

	pb "add_service/protos"
)

func main() {
	cfg := config.Load()

	add := cfg.GRPCHost + cfg.GRPCPort
	lis, err := net.Listen("tcp", add)
	if err != nil {
		panic(err)
	}

	pgStore, err := postgres.New(context.Background(), cfg)
	if err != nil {
		panic(err)
	}
	defer pgStore.Close()

	s := grpc.NewServer()

	addStore := postgres.NewAddService(pgStore.Pool)

	pb.RegisterCalculatorServicesServer(s, addStore)

	fmt.Println("listening at", add)
	if err = s.Serve(lis); err != nil {
		panic(err)
	}
}
